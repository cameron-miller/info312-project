/*

 */
package domain;

import java.sql.Time;
import java.util.Date;
import java.util.UUID;

/**
 *
 * @author cameron
 * @version 0.1
 */
public class Shift {

    private String id;
    private Time start;
    private Time finish;
    private Date date;
    private Employee employee;
    private String trainingReq;
    private double shiftHrs;

    public Shift(Time start, Time finish, Date date, Employee employee, String trainingReq) {
        this.id = UUID.randomUUID().toString();
        this.start = start;
        this.finish = finish;
        this.date = date;
        this.employee = employee;
        this.trainingReq = trainingReq;
        this.shiftHrs = (finish.getTime() - start.getTime()) / 3600000;
    }

    public Shift(Time start, Time finish, Date date, String trainingReq) {
        this.id = UUID.randomUUID().toString();
        this.start = start;
        this.finish = finish;
        this.date = date;
        this.trainingReq = trainingReq;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Time getStart() {
        return start;
    }

    public void setStart(Time start) {
        this.start = start;
    }

    public Time getFinish() {
        return finish;
    }

    public void setFinish(Time finish) {
        this.finish = finish;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void assign(Employee employee) {
        this.employee = employee;
    }

    public Boolean isAssigned() {
        if (employee == null) {
            return false;
        } else {
            return true;
        }
    }

    public String getTrainingReq() {
        return trainingReq;
    }

    public void setTrainingReq(String trainingReq) {
        this.trainingReq = trainingReq;
    }

    public double getShiftHrs() {
        return shiftHrs;
    }

    public void setShiftHrs(double shiftHrs) {
        this.shiftHrs = shiftHrs;
    }

    @Override
    public String toString() {
        return "Shift{" + "id=" + id + ", start=" + start + ", finish=" + finish + ", date=" + date + ", employee=" + employee + ", trainingReq=" + trainingReq + '}';
    }

}
