/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

/**
 *
 * @author milca826
 */
public class Roster {

    private Date start;
    private Date end;

    private ArrayList<Shift> shifts = new ArrayList<>();
    private ArrayList<Employee> employees;

    private  String[] weekdays = new String[7];

    //there will eventually have to be some support for storage of shift times
    //and reqs stored in a file. Even if two or three configs could be written
    //for now I will just use the roster given to us in examples.
    public Roster(Date start, Date end, ArrayList<Employee> employees) {
        this.start = start;
        this.end = end;
        this.employees = employees;
        //define weekdays for view objects
        weekdays[0] = "Sunday";
        weekdays[1] = "Monday";
        weekdays[2] = "Tuesday";
        weekdays[3] = "Wednesday";
        weekdays[4] = "Thursday";
        weekdays[5] = "Friday";
        weekdays[6] = "Saturday";
    }
    
    protected void populateShifts(){
        //for every date in the date range, add the appropriate shifts
        Date d = start;
        Calendar cal = Calendar.getInstance();
        while (d.compareTo(end) <= 0) {
            cal.setTime(d);
            if (cal.get(Calendar.DAY_OF_MONTH) == 0) {
                //if it's sunday, shifts start at 8.45 else 5.45
                shifts.add(new Shift(new Time(31500000), new Time(43200000), d, "E"));//8.45->12
                shifts.add(new Shift(new Time(32400000), new Time(43200000), d, "E"));//9->12
                shifts.add(new Shift(new Time(43200000), new Time(54000000), d, "E"));//12->3
                shifts.add(new Shift(new Time(43200000), new Time(54000000), d, "E"));//12->3
                shifts.add(new Shift(new Time(54000000), new Time(68400000), d, "E"));//3->7
                shifts.add(new Shift(new Time(54000000), new Time(68400000), d, "E"));//3->7
                shifts.add(new Shift(new Time(54000000), new Time(68400000), d, "E"));//3->7
                shifts.add(new Shift(new Time(68400000), new Time(82800000), d, "E"));//7->11
                shifts.add(new Shift(new Time(68400000), new Time(82800000), d, "E"));//7->11
                shifts.add(new Shift(new Time(68400000), new Time(82800000), d, "E"));//7->11
            } else {
                //normal shifts
                shifts.add(new Shift(new Time(19620000), new Time(32400000), d, "E"));//5.45->9
                shifts.add(new Shift(new Time(21600000), new Time(32400000), d, "E"));//6->9
                shifts.add(new Shift(new Time(32400000), new Time(43200000), d, "E"));//9->12
                shifts.add(new Shift(new Time(32400000), new Time(43200000), d, "E"));//9->12
                shifts.add(new Shift(new Time(43200000), new Time(54000000), d, "E"));//12->3
                shifts.add(new Shift(new Time(43200000), new Time(54000000), d, "E"));//12->3
                shifts.add(new Shift(new Time(54000000), new Time(68400000), d, "E"));//3->7
                shifts.add(new Shift(new Time(54000000), new Time(68400000), d, "E"));//3->7
                shifts.add(new Shift(new Time(54000000), new Time(68400000), d, "E"));//3->7
                shifts.add(new Shift(new Time(68400000), new Time(82800000), d, "E"));//7->11
                shifts.add(new Shift(new Time(68400000), new Time(82800000), d, "E"));//7->11
                shifts.add(new Shift(new Time(68400000), new Time(82800000), d, "E"));//7->11                                
            }
            d = new Date(d.getTime() + 86400000);
        }
    }

    public void autoFillShifts() {
        //try to put employees in the shifts they want
        //make sure employee constraints are still valid
        //cycle to ensure that all shifts have been assigned, then assign those shifts
        //TODO: random selection of employees for first pick
        // if an employee still ends up with less than min hours due to the algorithm failing, the user should be prompted to manually shuffle shifts
        for (Employee e : employees) {
            for (Shift shift : shifts) {
                if (!shift.isAssigned()) {
                    for (Shift eShift : e.getShiftsAvailable()) {
                        //if the shift is preferable and rules are satisfied
                        //TODO: add a list of rules to the user class
                        if (shift.getStart() == eShift.getStart() && shift.getFinish() == eShift.getFinish() & shift.getDate() == eShift.getDate()) {
                            //calculate shift hours to ensure that nobody is getting overworked
                            if (e.getCurrentHours() + shift.getShiftHrs() < e.getMaxhrs()) {
                                e.setCurrentHours(e.getCurrentHours() + shift.getShiftHrs());
                                shift.assign(e);
                            }
                        }
                    }
                }
            }
        }
        //try to fill empty shifts until minHrs has been satisfied
        for (Employee e : employees) {
            //if the employee has not satisfied minHrs
            if (e.getCurrentHours() < e.getMinhrs()) {
                for (Shift shift : shifts) {
                    if (!shift.isAssigned()) {
                        Boolean shiftOk = true;
                        while (shiftOk) {
                            for (Shift notShift : e.getImpossibleShifts()) {
                                //realistically if shift.equals(notshift)
                                if (shift.getStart() == notShift.getStart() && shift.getFinish() == notShift.getFinish() && shift.getDate() == notShift.getDate()) {
                                    shiftOk = false;
                                }
                            }
                        }
                        //
                        if (shiftOk && e.getCurrentHours() < e.getMinhrs()) {
                            shift.assign(e);
                        }
                    }
                }
            }
        }
        //random through employees and shifts until all filled ensuring that employees are still under max hrs
        ArrayList<Employee> employeesPicked = new ArrayList<>();
        //while all shifts have not been assigned.
        while(!allShiftsAssigned()){
            //pick a random shift
            Shift randomShift = shifts.get(randInt(0, employees.size()));
            //if that shift isnt assigned, pick a random employee
            //check that employee is available for that shift if true, then assign the employee, add to employeesPicked, repeat until employeesPicked is full, then iterate until no shifts left
        }
        
        
    }
    
    public Boolean allShiftsAssigned() {
        Boolean allAssigned = true;
        //cut down on comparisons because why not.
        while (allAssigned) {
            for (Shift shift : shifts) {
                if (!shift.isAssigned()) {
                    allAssigned = false;
                }
            }
        }
        return allAssigned;
    }
    
    /**
     * Returns a pseudo-random number between min and max, inclusive. The
     * difference between min and max can be at most
     * <code>Integer.MAX_VALUE - 1</code>.
     *
     * @param min Minimum value
     * @param max Maximum value. Must be greater than min.
     * @return Integer between min and max, inclusive.
     * @see java.util.Random#nextInt(int)
     */
    public int randInt(int min, int max) {

    // NOTE: Usually this should be a field rather than a method
        // variable so that it is not re-seeded every call.
        Random rand = new Random();

    // nextInt is normally exclusive of the top value,
        // so add 1 to make it inclusive
        int randomNum = rand.nextInt((max - min) + 1) + min;

        return randomNum;
    }

}
