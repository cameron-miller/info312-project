/*

 */

package domain;

import java.util.ArrayList;
import java.util.UUID;

/**
 *
 * @author cameron
 * @version 0.1
 */
public class Employee {
    
    private String id;
    private double maxhrs;
    private double minhrs;
    private String trainingLevel; //will have some constraints in DB
    private ArrayList<Shift> shiftsAvailable;
    private ArrayList<Shift> impossibleShifts; //when no other alternative exists
    private double currentHours; //for any given roster
    //employees will have some rules for special cases. will sort these out later

    public Employee(double maxhrs, double minhrs, String trainingLevel, ArrayList<Shift> shiftsAvailable, ArrayList<Shift> impossibleShifts, double currentHours) {
        this.id = UUID.randomUUID().toString();;
        this.maxhrs = maxhrs;
        this.minhrs = minhrs;
        this.trainingLevel = trainingLevel;
        this.shiftsAvailable = shiftsAvailable;
        this.impossibleShifts = impossibleShifts;
        this.currentHours = currentHours;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public double getMaxhrs() {
        return maxhrs;
    }

    public void setMaxhrs(double maxhrs) {
        this.maxhrs = maxhrs;
    }

    public double getMinhrs() {
        return minhrs;
    }

    public void setMinhrs(double minhrs) {
        this.minhrs = minhrs;
    }

    public String getTrainingLevel() {
        return trainingLevel;
    }

    public void setTrainingLevel(String trainingLevel) {
        this.trainingLevel = trainingLevel;
    }

    public ArrayList<Shift> getShiftsAvailable() {
        return shiftsAvailable;
    }

    public void setShiftsAvailable(ArrayList<Shift> shiftsAvailable) {
        this.shiftsAvailable = shiftsAvailable;
    }

    public ArrayList<Shift> getImpossibleShifts() {
        return impossibleShifts;
    }

    public void setImpossibleShifts(ArrayList<Shift> impossibleShifts) {
        this.impossibleShifts = impossibleShifts;
    }

    public double getCurrentHours() {
        return currentHours;
    }

    public void setCurrentHours(double currentHours) {
        this.currentHours = currentHours;
    }

    @Override
    public String toString() {
        return "Employee{" + "id=" + id + ", maxhrs=" + maxhrs + ", minhrs=" + minhrs + ", trainingLevel=" + trainingLevel + ", shiftsAvailable=" + shiftsAvailable + ", impossibleShifts=" + impossibleShifts + ", currentHours=" + currentHours + '}';
    }
    
    
    

}
